package cat.itb.retrofit;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cat.itb.retrofit.model.Data;
import cat.itb.retrofit.model.Pelicula;
import cat.itb.retrofit.web_service.WebServiceClient;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FilmDetailsActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private HttpLoggingInterceptor interceptor;
    private OkHttpClient.Builder httpClientBuilder;
    private TextView title, episode, crawl;
    private Button next, previous;
    private int filmPosition;
    private char[] filmIDs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.film_details_activity);

        next = findViewById(R.id.nextFilmButton);
        previous = findViewById(R.id.previousFilmButton);
        title = findViewById(R.id.titleTextView);
        episode = findViewById(R.id.episodeTextView);
        crawl = findViewById(R.id.crawlTextView);
        filmPosition = 0;

        previous.setVisibility(View.INVISIBLE);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String[] films = bundle.getStringArray("films");
            filmIDs = new char[films.length];
            for (int i = 0; i < films.length; i++) {
                filmIDs[i] = films[i].charAt(films[i].length() - 2);
            }
        }
        lanzarPeticion(filmIDs[filmPosition]);

        next.setOnClickListener(v -> {
            previous.setVisibility(View.VISIBLE);
            filmPosition++;
            lanzarPeticion(filmIDs[filmPosition]);
        });

        previous.setOnClickListener(v ->{
            next.setVisibility(View.VISIBLE);
            filmPosition--;
            lanzarPeticion(filmIDs[filmPosition]);
        });
    }

    private void lanzarPeticion(char i){
        interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(interceptor);

        retrofit = new Retrofit.Builder().baseUrl("https://swapi.dev/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();

        WebServiceClient client = retrofit.create(WebServiceClient.class);
        Call<Pelicula> call;
        call = client.getPelicula(i);
        call.enqueue(new Callback<Pelicula>() {

            @Override
            public void onResponse(Call<Pelicula> call, Response<Pelicula> response) {
                title.setText(response.body().getTitle());
                episode.setText("Episode " + response.body().getEpisode_id());
                crawl.setText(response.body().getOpening_crawl());

                if(filmPosition == filmIDs.length - 1) next.setVisibility(View.INVISIBLE);
                else if(filmPosition == 0) previous.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<Pelicula> call, Throwable t) {

            }
        });
    }
}
