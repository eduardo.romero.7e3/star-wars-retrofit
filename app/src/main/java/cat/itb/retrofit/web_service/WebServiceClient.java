package cat.itb.retrofit.web_service;

import cat.itb.retrofit.model.Data;
import cat.itb.retrofit.model.Pelicula;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WebServiceClient {

    @GET("people/")
    Call<Data> getPersonajes(@Query("page") int p);

    @GET("people/")
    Call<Data> getPersonajes(@Query("page") int p, @Query("search") String s);

    @GET("films/{s}")
    Call<Pelicula> getPelicula(@Path("s") char s);
}
