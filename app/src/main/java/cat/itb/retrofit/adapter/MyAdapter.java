package cat.itb.retrofit.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.retrofit.FilmDetailsActivity;
import cat.itb.retrofit.MainActivity;
import cat.itb.retrofit.R;
import cat.itb.retrofit.model.Personaje;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Holder> {

    static List<Personaje> personajes;

    public MyAdapter(List<Personaje> personajes) {
        MyAdapter.personajes = personajes;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Personaje p = personajes.get(position);
        holder.textViewNameValue.setText(p.getName());
        holder.textViewHeightValue.setText(p.getHeight());
        holder.textViewSkinValue.setText(p.getskin());
    }

    @Override
    public int getItemCount() {
        return personajes.size();
    }

    public void setPersonajes(List<Personaje> personajes) {
        MyAdapter.personajes = personajes;
        notifyDataSetChanged();
    }

    public static class Holder extends RecyclerView.ViewHolder{
        TextView textViewNameValue;
        TextView textViewHeightValue;
        TextView textViewSkinValue;
        
        public Holder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), FilmDetailsActivity.class);
                intent.putExtra("films", personajes.get(getAdapterPosition()).getFilms());
                v.getContext().startActivity(intent);
            });

            textViewNameValue = itemView.findViewById(R.id.textViewNameValue);
            textViewHeightValue = itemView.findViewById(R.id.textViewHeightValue);
            textViewSkinValue = itemView.findViewById(R.id.textViewSkinValue);

        }
    }
}
