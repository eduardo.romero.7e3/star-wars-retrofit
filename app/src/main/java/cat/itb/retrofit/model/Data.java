package cat.itb.retrofit.model;

import java.util.List;

public class Data{
    private int count;
    private String next;
    private String previous;
    private List<Personaje> results;

    public int getCount() {
        return count;
    }

    public List<Personaje> getResults() {
        return results;
    }

}
