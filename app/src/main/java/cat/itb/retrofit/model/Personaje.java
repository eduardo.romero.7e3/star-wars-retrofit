package cat.itb.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class Personaje {
    private String name;
    private String height;
    @SerializedName("skin_color") // Indica el nombre del campo en el JSON
    private String skin;
    private String[] films;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getskin() {
        return skin;
    }

    public void setskin(String skin) {
        this.skin = skin;
    }

    public String[] getFilms() {
        return films;
    }

    public void setFilms(String[] films) {
        this.films = films;
    }
}
