package cat.itb.retrofit.model;

public class Pelicula {
    private String title;
    private int episode_id;
    private String opening_crawl;

    public Pelicula(String title, int episode_id, String opening_crawl) {
        this.title = title;
        this.episode_id = episode_id;
        this.opening_crawl = opening_crawl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEpisode_id() {
        return episode_id;
    }

    public void setEpisode_id(int episode_id) {
        this.episode_id = episode_id;
    }

    public String getOpening_crawl() {
        return opening_crawl;
    }

    public void setOpening_crawl(String opening_crawl) {
        this.opening_crawl = opening_crawl;
    }
}
