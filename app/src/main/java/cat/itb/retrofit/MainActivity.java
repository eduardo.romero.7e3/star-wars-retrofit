package cat.itb.retrofit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cat.itb.retrofit.adapter.MyAdapter;
import cat.itb.retrofit.model.Data;
import cat.itb.retrofit.model.Personaje;
import cat.itb.retrofit.web_service.WebServiceClient;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MyAdapter adapter;
    private List<Personaje> personajes;
    private Retrofit retrofit;
    private HttpLoggingInterceptor interceptor;
    private OkHttpClient.Builder httpClientBuilder;
    private EditText filterEditText;
    private Button previousButton, nextButton, filterButton;
    private int page, count;
    private Boolean filtered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        personajes = new ArrayList<Personaje>();
        adapter = new MyAdapter(personajes);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        previousButton = findViewById(R.id.previousButton);
        nextButton = findViewById(R.id.nextButton);
        filterButton = findViewById(R.id.filterButton);
        filterEditText = findViewById(R.id.filterEditText);
        page = 1;
        filtered = false;
        previousButton.setVisibility(View.INVISIBLE);

        lanzarPeticion(filtered);

        previousButton.setOnClickListener(v -> {
            if(page > 1) {
                page--;
            }
            lanzarPeticion(filtered);
            nextButton.setVisibility(View.VISIBLE);
        });

        nextButton.setOnClickListener(v -> {
            if(page < count / 10 + 1) {
                page++;
            }
            lanzarPeticion(filtered);
            previousButton.setVisibility(View.VISIBLE);
        });

        filterButton.setOnClickListener(v -> {
            page = 1;
            nextButton.setVisibility(View.VISIBLE);
            if(!filterEditText.getText().toString().isEmpty()){
                filtered = true;
            }
            else filtered = false;
            lanzarPeticion(filtered);
        });
    }

    private void lanzarPeticion(boolean filtered){
        interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(interceptor);

        retrofit = new Retrofit.Builder().baseUrl("https://swapi.dev/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();

        WebServiceClient client = retrofit.create(WebServiceClient.class);
        Call<Data> call;
        if(!filtered) call = client.getPersonajes(page);
        else call = client.getPersonajes(page, filterEditText.getText().toString());
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                personajes = response.body().getResults();
                adapter.setPersonajes(personajes);
                count = response.body().getCount();
                if(count % 10 != 0 && page == count / 10 + 1 || count % 10 == 0 && page == count / 10){
                    nextButton.setVisibility(View.INVISIBLE);
                }
                if(page == 1){
                    previousButton.setVisibility(View.INVISIBLE);
                }
                /* Intenté hacerlo con response.body().getNext() y getPrevious(), pero por algún
                 motivo el if(getNext() == null) nunca devolvía true, así que tiré por esta vía */
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                Log.d("TAG1", "Mensaje: " + t.getMessage());
            }
        });
    }

    //TODO: RecyclerView OnClickListener

}